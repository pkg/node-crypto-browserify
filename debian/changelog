node-crypto-browserify (3.12.0-7+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 08 Mar 2025 15:33:30 +0000

node-crypto-browserify (3.12.0-7) unstable; urgency=medium

  * Team upload
  * Update lintian override info format in d/source/lintian-overrides
    on line 2-7
  * Declare compliance with policy 4.7.0
  * Update homepage

 -- Yadd <yadd@debian.org>  Mon, 10 Jun 2024 17:31:02 +0400

node-crypto-browserify (3.12.0-6+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 21:04:45 +0000

node-crypto-browserify (3.12.0-6) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.1
  * Disable RIPEMD-160 protocol (Closes: #1011802)

 -- Yadd <yadd@debian.org>  Wed, 01 Jun 2022 10:59:31 +0200

node-crypto-browserify (3.12.0-5) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Yadd ]
  * Declare compliance with policy 4.6.0
  * Add "Rules-Requires-Root: no"
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Update lintian overrides
  * Use dh-sequence-nodejs auto test & install
  * Drop dependency to nodejs

 -- Yadd <yadd@debian.org>  Mon, 01 Nov 2021 11:53:46 +0100

node-crypto-browserify (3.12.0-4) unstable; urgency=medium

  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 28 Aug 2021 21:10:49 +0100

node-crypto-browserify (3.12.0-3co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sun, 21 Feb 2021 10:29:08 +0000

node-crypto-browserify (3.12.0-3) unstable; urgency=medium

  * Remove node-diffie-hellman dependency (last upload only removed build
    dependency)

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Mar 2018 17:06:00 +0530

node-crypto-browserify (3.12.0-2) unstable; urgency=medium

  * Remove diffie hellman support (its insecure, see #860939)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Mar 2018 17:40:36 +0530

node-crypto-browserify (3.12.0-1) unstable; urgency=medium

  * New upstream version 3.12.0
  * Add node-randomfill as dependency
  * BUmp standards version to 4.1.3 and debhelper compat to 11

 -- Pirate Praveen <praveen@debian.org>  Tue, 27 Feb 2018 12:46:14 +0530

node-crypto-browserify (3.11.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.11.1
  * Standards-Version: 4.1.2
  * Section: javascript
  * Add test to work around unsupported ciphers in openssl

 -- Jérémy Lal <kapouer@melix.org>  Sat, 30 Dec 2017 11:17:38 +0100

node-crypto-browserify (3.11.0-1) unstable; urgency=low

  * Initial release (Closes: #864893)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Jun 2017 21:09:03 +0530
